from main import bot


@bot.message_handler(commands=['start'])
def index_handler(msg):
    bot.send_message(msg.chat.id, 'Hello, {}! This bot do nothing useful yet!'
                     .format(msg.from_user.first_name))


@bot.message_handler(commands=['ping'])
def ping_handler(msg):
    bot.send_message(msg.chat.id, 'pong!')


@bot.message_handler(commands=['quit'])
def ping_handler(msg):
    bot.send_message(msg.chat.id, 'Bye!')
