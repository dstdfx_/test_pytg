import time

from datetime import datetime
from threading import Thread

from pytg.receiver import Receiver
from pytg.sender import Sender
from pytg.utils import coroutine

start_time, end_time = None, None


def check_every_ten_seconds(sender):
    global start_time
    for _ in range(10):
        start_time = datetime.now()
        sender.send_msg('@echtest_bot', '/ping')
        time.sleep(2)


def main():
    receiver = Receiver(host="localhost", port=4458)
    sender = Sender(host="localhost", port=4458)

    Thread(target=check_every_ten_seconds, args=(sender, )).start()

    receiver.start()

    # function as message listener. You can supply arguments here (like sender).
    receiver.message(main_loop(sender))

    receiver.stop()


@coroutine
def main_loop(sender):
    global start_time, end_time
    quit = False
    try:
        while not quit:  # loop for messages
            msg = (yield)  # it waits until the generator has a has message here.
            sender.status_online()  # so we will stay online.
            # print(msg)

            if msg.event != "message":
                continue  # is not a message.
            if msg.own:  # the bot has send this message.
                continue  # we don't want to process this message.
            if msg.text is None:  # we have media instead.
                continue  # and again, because we want to process only text message.
            if msg.text == "pong!":
                end_time = datetime.now()
                res_time = end_time - start_time
                print('Bot is okay! Request timing : {} sec {} msec'
                      .format(res_time.seconds, res_time.microseconds))
            if msg.text == 'Bye!':
                quit = True

    except GeneratorExit:
        # the generator (pytg) exited (got a KeyboardIterrupt).
        pass
    except KeyboardInterrupt:
        # we got a KeyboardIterrupt(Ctrl+C)
        pass
    else:
        pass


if __name__ == '__main__':
    main()
