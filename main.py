import time
import os
import cherrypy

from telebot import TeleBot, types

bot = TeleBot(os.environ['TELEGRAM_TOKEN'])


class WebhookServer(object):
    @cherrypy.expose
    def index(self):
        if 'content-length' in cherrypy.request.headers and \
                        'content-type' in cherrypy.request.headers and \
                        cherrypy.request.headers['content-type'] == 'application/json':
            length = int(cherrypy.request.headers['content-length'])
            json_string = cherrypy.request.body.read(length).decode("utf-8")
            update = types.Update.de_json(json_string)
            bot.process_new_updates([update])

            return ''
        else:
            raise cherrypy.HTTPError(403)


if __name__ == '__main__':

    bot.remove_webhook()
    time.sleep(1)
    bot.set_webhook(url=os.environ['WEBHOOK_URL'])

    from bot.handlers import *

    cherrypy.config.update({
        'server.socket_host': '0.0.0.0',
        'server.socket_port': 8085
    })

    cherrypy.quickstart(WebhookServer(), '/')
